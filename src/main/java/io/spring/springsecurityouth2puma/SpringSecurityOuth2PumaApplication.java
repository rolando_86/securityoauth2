package io.spring.springsecurityouth2puma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@SpringBootApplication
public class SpringSecurityOuth2PumaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityOuth2PumaApplication.class, args);
	}
}

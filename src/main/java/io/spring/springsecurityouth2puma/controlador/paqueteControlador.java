package io.spring.springsecurityouth2puma.controlador;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api")
public class paqueteControlador {

    @GetMapping("/publico/paquete")
    public String obtenerPaquetes(){
        return "regalos 1 - regalos 2 - regalos 3";
    }
    @GetMapping("/publico/regalos")
    public String obtenerRegalos(){
        return "juguetes - ropa - sorpresa";
    }
    @GetMapping("/publico/sorpresa")
    public String obtenerSorpresas(){
        return "viaje - auto - vacaciones pagas";
    }
    @GetMapping("/privado/usuario")
    public String obtenerUsuarios(){
        return "admin - pepe55 - Rolo33";
    }
    @GetMapping("/privado/proveedor")
    public String obtenerProveedores(){
        return "proveedor 1 - proveedor 2 - proveedor 3";
    }

    @GetMapping("/publico/user")
    public Principal user(Principal principal){
        return principal;
    }
}
